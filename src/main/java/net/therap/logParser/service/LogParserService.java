package net.therap.logParser.service;

import net.therap.logParser.model.Log;
import net.therap.logParser.model.LogSummary;

import java.io.File;
import java.util.*;
import java.util.regex.Pattern;

/**
 * @author sakib.khan
 * @since 12/12/21
 */
public class LogParserService {

    static final String TIME_REGEX = "(\\d){2}[:](\\d){2}[:](\\d){2}";
    static final String RESPONSE_TIME_REGEX = "(\\d)+[m][s]";
    static final String URI_REGEX = "[/][\\w/]+";
    static final String GET_REQUEST_REGEX = "G";
    static final String POST_REQUEST_REGEX = "P";

    public List<Log> getLogList(String logFilePath) {
        List<Log> logs = new ArrayList<>();
        if (Objects.isNull(logFilePath)) return logs;
        try {
            File logFile = new File(logFilePath);
            Scanner scan = new Scanner(logFile);
            while (scan.hasNextLine()) {
                Log newLog = getTokenizedLog(scan.nextLine());
                logs.add(newLog);
            }
        } catch (Exception e) {
            System.out.println("Processing Stopped With Exception : " + e);
        }
        return logs;
    }

    private Log getTokenizedLog(String line) {

        double startTime = 0.00;
        double endTime = 0.00;
        String httpRequest = null;
        String uri = null;
        int responseTime = 0;

        StringTokenizer stringTokenizer = new StringTokenizer(line, " []'',=");

        while (stringTokenizer.hasMoreTokens()) {
            String token = stringTokenizer.nextToken();
            if (Pattern.matches(TIME_REGEX, token)) {
                startTime = Integer.parseInt(token.split(":")[0]);
                endTime = startTime + 1;
            } else if (Pattern.matches(GET_REQUEST_REGEX, token)) {
                httpRequest = "get";
            } else if (Pattern.matches(POST_REQUEST_REGEX, token)) {
                httpRequest = "post";
            } else if (Pattern.matches(URI_REGEX, token)) {
                uri = token;
            } else if (Pattern.matches(RESPONSE_TIME_REGEX, token)) {
                responseTime = Integer.parseInt(token.split("ms")[0]);
            }
        }
        return new Log(startTime, endTime, httpRequest, uri, responseTime);
    }

    public HashMap<Integer, LogSummary> processLogs(List<Log> logList) {

        HashMap<Integer, LogSummary> logSummaryHashMap = new HashMap<>();

        for (Log log : logList) {

            int logHashKey = (int) log.getStartTime();

            LogSummary logSummary = this.getLogSummary(logSummaryHashMap, logHashKey);

            if ("get".equals(log.getHttpRequest())) {
                int newGetCount = logSummary.getGetRequestCount() + 1;
                logSummary.setGetRequestCount(newGetCount);
            } else if ("post".equals(log.getHttpRequest())) {
                int newPostCount = logSummary.getPostRequestCount() + 1;
                logSummary.setPostRequestCount(newPostCount);
            }

            if (!Objects.isNull(log.getUri())){
                logSummary.addUri(log.getUri());
            }

            int totalResponseTime = logSummary.getTotalResponseTime() + log.getTotalResponseTime();
            logSummary.setTotalResponseTime(totalResponseTime);

            logSummaryHashMap.put(logHashKey, logSummary);
        }
        return logSummaryHashMap;
    }


    public Map<Integer, LogSummary> sortLogSummary(Map<Integer, LogSummary> logSummaryHashMap) {

        Set<Map.Entry<Integer, LogSummary>> logSummaryEntrySet = logSummaryHashMap.entrySet();
        List<Map.Entry<Integer, LogSummary>> logSummaryEntryList = new ArrayList<>(logSummaryEntrySet);
        Map<Integer, LogSummary> sortedLogSummary = new LinkedHashMap<Integer, LogSummary>();

        Collections.sort(logSummaryEntryList, (logSummaryEntry1, logSummaryEntry2) -> {
            int entry1TotalRequest = logSummaryEntry1.getValue().getGetRequestCount() + logSummaryEntry1.getValue().getPostRequestCount();
            int entry2TotalRequest = logSummaryEntry2.getValue().getGetRequestCount() + logSummaryEntry2.getValue().getPostRequestCount();
            return entry1TotalRequest - entry2TotalRequest;
        });

        logSummaryEntryList.forEach(item -> {
            sortedLogSummary.put(item.getKey(), item.getValue());
        });

        return sortedLogSummary;
    }

    private LogSummary getLogSummary(HashMap<Integer, LogSummary> logSummaryHashMap, int logHashKey) {

        LogSummary logSummary;

        if (logSummaryHashMap.containsKey(logHashKey)) {
            logSummary = logSummaryHashMap.get(logHashKey);
        } else {
            logSummary = new LogSummary(logHashKey);
        }

        return logSummary;
    }
}