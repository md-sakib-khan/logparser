package net.therap.logParser.model;

import java.util.HashSet;
import java.util.Set;

/**
 * @author sakib.khan
 * @since 12/12/21
 */
public class LogSummary{

    private int logNumberByHour;
    private int getRequestCount;
    private int postRequestCount;
    private Set<String> uriSet;
    private int totalResponseTime;

    public LogSummary(int logHashKey){
        logNumberByHour = logHashKey;
        getRequestCount = 0;
        postRequestCount = 0;
        totalResponseTime = 0;
        uriSet = new HashSet<>();
    }

    public int getLogNumberByHour() {
        return logNumberByHour;
    }

    public void setLogNumberByHour(int logNumberByHour) {
        this.logNumberByHour = logNumberByHour;
    }

    public void setGetRequestCount(int newGetCount) {
        this.getRequestCount = newGetCount;
    }

    public int getPostRequestCount() {
        return postRequestCount;
    }

    public void setPostRequestCount(int newPostCount) {
        this.postRequestCount = newPostCount;
    }

    public int getGetRequestCount() {
        return getRequestCount;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    public void addUri(String uri){
        this.uriSet.add(uri);
    }

    public int getUniqueUriCount() {
        return uriSet.size();
    }
}
