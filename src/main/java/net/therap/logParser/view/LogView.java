package net.therap.logParser.view;

import net.therap.logParser.model.LogSummary;

import java.util.Map;

/**
 * @author sakib.khan
 * @since 12/13/21
 */
public class LogView {

    private static final int HOUR_FORMAT_LIMIT = 12;

    private static final String OUTPUT_FORMAT = "%2d" + "%s" + "-" + "%d" + "%s" + " " + "%10d" + "/" + "%2d" + "%18d" + " " + "%20d" + "\n";

    public void showSummary(Map<Integer, LogSummary> summaryList) {

        System.out.format("%7s  %18s  %20s  %20s\n", "Time", "GET/POST Count", "Unique URI Count", "Total Response Time");

        summaryList.forEach((hashKey, summary) -> {

            int startHour = summary.getLogNumberByHour();
            int endHour = startHour + 1;
            int getRequestCount = summary.getGetRequestCount();
            int postRequestCount = summary.getPostRequestCount();
            int uniqueCharCount = summary.getUniqueUriCount();
            int totalResponseTime = summary.getTotalResponseTime();

            String startHourFormat = "am";
            String endHourFormat = "am";

            if (startHour >= HOUR_FORMAT_LIMIT) {
                if (startHour > HOUR_FORMAT_LIMIT) {
                    startHour = startHour - HOUR_FORMAT_LIMIT;
                }
                startHourFormat = "pm";
            }
            if (endHour >= HOUR_FORMAT_LIMIT) {
                if (endHour > HOUR_FORMAT_LIMIT) {
                    endHour = endHour - HOUR_FORMAT_LIMIT;
                }
                endHourFormat = "pm";
            }

            System.out.format(OUTPUT_FORMAT, startHour, ".00"+startHourFormat, endHour, ".00"+endHourFormat,
                    getRequestCount, postRequestCount, uniqueCharCount, totalResponseTime);
        });
    }
}
