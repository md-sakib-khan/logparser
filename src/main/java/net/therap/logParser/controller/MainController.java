package net.therap.logParser.controller;

/**
 * @author sakib.khan
 * @since 12/12/21
 */
public class MainController {

    public static void main(String[] args) {
        LogParserController logParserController = new LogParserController();
        logParserController.init(args);
    }
}
