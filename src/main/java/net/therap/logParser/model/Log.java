package net.therap.logParser.model;

/**
 * @author sakib.khan
 * @since 12/12/21
 */
public class Log {

    private double endTime;
    private double startTime;
    private String httpRequest;
    private String uri;
    private int totalResponseTime;

    public Log(double startTime, double endTime, String httpRequest, String uri, int totalResponseTime){
        this.startTime = startTime;
        this.endTime = endTime;
        this.httpRequest = httpRequest;
        this.uri = uri;
        this.totalResponseTime = totalResponseTime;
    }

    public double getEndTime() {
        return endTime;
    }

    public void setEndTime(double endTime) {
        this.endTime = endTime;
    }

    public double getStartTime() {
        return startTime;
    }

    public void setStartTime(double startTime) {
        this.startTime = startTime;
    }

    public String getUri() {
        return uri;
    }

    public void setUni(String uri) {
        this.uri = uri;
    }

    public int getTotalResponseTime() {
        return totalResponseTime;
    }

    public void setTotalResponseTime(int totalResponseTime) {
        this.totalResponseTime = totalResponseTime;
    }

    public String getHttpRequest() {
        return httpRequest;
    }

    public void setHttpRequest(String httpRequest) {
        this.httpRequest = httpRequest;
    }
}