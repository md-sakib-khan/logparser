package net.therap.logParser.controller;

import net.therap.logParser.model.Log;
import net.therap.logParser.model.LogSummary;
import net.therap.logParser.service.LogParserService;
import net.therap.logParser.view.LogView;

import java.util.*;

/**
 * @author sakib.khan
 * @since 12/12/21
 */
public class LogParserController {

    private LogParserService logParserService;
    private LogView logView;

    private static final String SORT_KEY = "--sort";

    LogParserController() {
        logParserService = new LogParserService();
        logView = new LogView();
    }

    public void init(String[] args) {

        if (args.length == 0) {
            System.out.println("No Arguments Found");
            return;
        }

        boolean sort = (args.length == 2) && (SORT_KEY.equalsIgnoreCase(args[1]));

        String logFilePath = args[0];

        List<Log> logList = logParserService.getLogList(logFilePath);

        Map<Integer, LogSummary> logSummaryHashMap = logParserService.processLogs(logList);

        if (sort) {
            logSummaryHashMap = logParserService.sortLogSummary(logSummaryHashMap);
        }

        logView.showSummary(logSummaryHashMap);
    }
}
